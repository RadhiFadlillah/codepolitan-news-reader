CodePolitan News Reader
=====================
CodePolitan News Reader merupakan aplikasi *news reader* untuk situs [CodePolitan](http://www.codepolitan.com/). CodePolitan sendiri adalah media online yang ditujukan untuk programmer Indonesia sebagai tempat bacaan dan sumber informasi dunia pemrograman dan teknologi. 

Aplikasi ini dikembangkan untuk event [Dicoding Challenge](https://www.dicoding.com/challenges/35) tentang Codepolitan News Reader App, yang berlangsung mulai dari 30 Juli 2015 sampai 30 September 2015, dan dapat diunduh di [Play Store](https://play.google.com/store/apps/details?id=com.cp.reader)

Fitur-Fitur
--------------
Fitur utama dari aplikasi ini adalah :

- Akses berita terbaru baik berdasarkan kategori maupun tag
- Akses nyanKomik, meme dan quote motivasi dari berbagai developer
- Proses navigasi antar artikel yang mudah, cukup dengan swipe ke kiri dan kanan
- Pencarian berita berdasarkan keyword yang ditentukan
- Bookmark berita yang disukai

Selain fitur utama di atas, aplikasi ini juga memiliki keunggulan sebagai berikut :

- Dapat digunakan pada device dengan OS Android Jelly Bean sampai Lollipop
- Dapat digunakan secara landscape maupun potrait sesuai dengan keinginan pengguna
- Sudah menerapkan konsep Material Design (walaupun tidak 100%)
- Sudah dioptimalisasi untuk perangkat tablet

Data Source
----------------
Aplikasi ini mengambil data dari API yang disediakan oleh CodePolitan untuk 
event Dicoding Challenge.

Libraries
------------
Dalam pengembangan aplikasi ini, digunakan beberapa library, yaitu :

- [Butter Knife](https://jakewharton.github.io/butterknife/) by [Jake Wharton](http://jakewharton.com/), digunakan untuk injeksi *view*
- [Material Dialogs](https://github.com/afollestad/material-dialogs) by [Aidan Follestad](https://github.com/afollestad), digunakan untuk menampilkan dialog yang sesuai dengan *Material UI guidelines*
- [Retrofit v1.9.0](https://square.github.io/retrofit/) by [Square](https://square.github.io/), digunakan untuk menerjemahkan web API ke dalam bentuk *interface* Java
- [OkHTTP](https://square.github.io/okhttp/) by [Square](https://square.github.io/), digunakan untuk membuat *HTTP client* yang kemudian digunakan oleh Retrofit
- [Otto](https://square.github.io/otto/) by [Square](https://square.github.io/), digunakan untuk membuat *event bus*
- [Joda Time](http://www.joda.org/joda-time/), digunakan untuk memformat jam dan tanggal
- [Glide](https://github.com/bumptech/glide) by [Bump Technologies](https://github.com/bumptech), digunakan untuk meload dan mencache gambar
- [Parceler](https://github.com/johncarl81/parceler) by [John Erickson](https://github.com/johncarl81), digunakan untuk membuat dan mengekstrak *parcelable* dengan mudah
- [Icepick](https://github.com/frankiesardo/icepick) by [Frankie Sardo](https://github.com/frankiesardo), digunakan untuk mengurangi kode untuk proses *save* dan *load* instance di Android

Lisensi
---------
*Source code* aplikasi ini disebar di bawah lisensi [GPLv3](http://choosealicense.com/licenses/gpl-3.0/) sehingga setiap orang dapat memodifikasi dan menyebarkannya, dengan syarat *souce code* tetap dibuka dan tetap dirilis di bawah lisensi GPLv3.

```
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
```