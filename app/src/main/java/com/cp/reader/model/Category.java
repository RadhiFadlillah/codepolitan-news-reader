package com.cp.reader.model;

import org.parceler.Parcel;

import java.util.List;

public class Category {
    public int code;
    public List<Result> result;

    @Parcel
    public static class Result {
        public String name;
        public String slug;
    }
}
