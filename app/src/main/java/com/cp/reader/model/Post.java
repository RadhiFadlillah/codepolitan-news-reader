package com.cp.reader.model;

import com.google.gson.annotations.SerializedName;
import org.parceler.Parcel;

import java.util.List;

@Parcel
public class Post {
    public int code;
    public List<Result> result;

    @Parcel
    public static class Result {
        public int id;
        public String title;
        public String excerpt;
        public String date;
        public String link;
        @SerializedName("thumbnail_small") public String thumbnailSmall;
        @SerializedName("thumbnail_medium") public String thumbnailMedium;
    }

}
