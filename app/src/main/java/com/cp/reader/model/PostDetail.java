package com.cp.reader.model;

import com.google.gson.annotations.SerializedName;
import org.parceler.Parcel;

import java.util.List;

public class PostDetail {
    public int code;
    public Result result;

    @Parcel
    public static class Result {
        public int id;
        public String title;
        public String content;
        public String date;
        public String link;
        public List<Category.Result> category;
        public List<Tag.Result> tags;
        @SerializedName("thumbnail_small") public String thumbnailSmall;
    }
}
