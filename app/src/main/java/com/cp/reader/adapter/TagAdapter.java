package com.cp.reader.adapter;

import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.cp.reader.R;
import com.cp.reader.listener.OnItemClickListener;
import com.cp.reader.model.Tag;

import java.util.List;

public abstract class TagAdapter extends BaseAdapter {
    private List<Tag.Result> listTag;

    public TagAdapter(List<Tag.Result> listTag) {
        this.listTag = listTag;
    }

    @Override
    public int getItemCount() {
        if (footerType != NO_FOOTER) return listTag.size() + 1;
        else return listTag.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        if (viewType == ITEM) {
            View v = inflater.inflate(R.layout.row_list_tag, parent, false);
            return new VHItem(v, itemClickListener);
        } else {
            return super.onCreateViewHolder(parent, viewType);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int type = getItemViewType(position);

        if (type == RETRY) return;
        if (type == PROGRESS) loadData();
        else {
            VHItem vhItem = (VHItem) holder;
            Tag.Result tag = listTag.get(position);
            vhItem.txtTag.setText(Html.fromHtml(tag.name).toString().toLowerCase());
            vhItem.txtTagCount.setText(String.format("%d post", tag.count));
        }
    }

    public void replaceData(List<Tag.Result> listTag) {
        this.listTag = listTag;
        notifyDataSetChanged();
    }

    public static class VHItem extends RecyclerView.ViewHolder {
        private OnItemClickListener itemClickListener;

        @Bind(R.id.txt_tag) TextView txtTag;
        @Bind(R.id.txt_tag_count) TextView txtTagCount;

        @OnClick(R.id.item_tag) public void itemClick() {
            itemClickListener.onItemClick(getAdapterPosition());
        }

        public VHItem(View view, OnItemClickListener itemClickListener) {
            super(view);
            ButterKnife.bind(this, view);
            this.itemClickListener = itemClickListener;
        }
    }
}
