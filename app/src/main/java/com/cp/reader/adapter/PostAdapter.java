package com.cp.reader.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.cp.reader.R;
import com.cp.reader.listener.OnItemClickListener;
import com.cp.reader.model.Post;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.List;
import java.util.Locale;

public abstract class PostAdapter extends BaseAdapter {
    private Context context;
    private List<Post.Result> listPost;

    private DateTimeFormatter dateFormatter;
    private Locale indonesia;

    public PostAdapter(Context context, List<Post.Result> listPost) {
        this.context = context;
        this.listPost = listPost;

        this.dateFormatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
        this.indonesia = new Locale("in_ID");
    }

    @Override
    public int getItemCount() {
        if (footerType > -1) return listPost.size() + 1;
        else return listPost.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        if (viewType == ITEM) {
            View v = inflater.inflate(R.layout.row_list_post, parent, false);
            return new VHItem(v, itemClickListener);
        } else {
            return super.onCreateViewHolder(parent, viewType);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int type = getItemViewType(position);

        if (type == RETRY) return;
        if (type == PROGRESS) loadData();
        else {
            VHItem vhItem = (VHItem) holder;

            // Atur background
            vhItem.cardView.setCardBackgroundColor(Color.WHITE);

            // Tampilkan tanggal
            Post.Result post = listPost.get(position);
            LocalDateTime dateTime = LocalDateTime.parse(post.date, dateFormatter);
            vhItem.txtPostDate.setText(dateTime.toString("dd MMMM yyyy, HH:mm:ss", indonesia));

            // Tampilkan post title
            vhItem.txtPostTitle.setText(Html.fromHtml(post.title));

            // Load image
            Glide.with(context)
                    .load(post.thumbnailMedium)
                    .placeholder(R.color.background)
                    .centerCrop()
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(vhItem.imgThumbnail);
        }
    }

    public void replaceData(List<Post.Result> listPost) {
        this.listPost = listPost;
        notifyDataSetChanged();
    }

    public static class VHItem extends RecyclerView.ViewHolder {
        private OnItemClickListener itemClickListener;

        @Bind(R.id.card_view) CardView cardView;
        @Bind(R.id.img_thumbnail) ImageView imgThumbnail;
        @Bind(R.id.txt_post_date) TextView txtPostDate;
        @Bind(R.id.txt_post_title) TextView txtPostTitle;

        @OnClick(R.id.item_post) public void itemClick() {
            itemClickListener.onItemClick(getAdapterPosition());
        }

        public VHItem(View view, OnItemClickListener itemClickListener) {
            super(view);
            ButterKnife.bind(this, view);
            this.itemClickListener = itemClickListener;
        }
    }
}
