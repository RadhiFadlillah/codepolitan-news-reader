package com.cp.reader.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.cp.reader.R;
import com.cp.reader.listener.OnItemClickListener;

public abstract class BaseAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    protected int footerType = NO_FOOTER;

    protected static final int ITEM = 0;
    protected static final int PROGRESS = 1;
    protected static final int RETRY = 2;

    public static final int NO_FOOTER = -1;
    public static final int FOOTER_PROGRESS = 0;
    public static final int FOOTER_RETRY = 1;

    @Override
    public int getItemViewType(int position) {
        if (position == getItemCount() - 1) {
            if (footerType == FOOTER_PROGRESS) return PROGRESS;
            else if (footerType == FOOTER_RETRY) return RETRY;
        }

        return ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case RETRY:
                View v1 = inflater.inflate(R.layout.row_list_retry, parent, false);
                return new VHRetry(v1, retryClickListener);
            default:
                View v2 = inflater.inflate(R.layout.row_list_progress, parent, false);
                return new VHProgress(v2);
        }
    }

    public void setFooterType(int footerType) {
        this.footerType = footerType;
        notifyDataSetChanged();
    }

    public static class VHProgress extends RecyclerView.ViewHolder {
        public VHProgress(View view) {
            super(view);
        }
    }

    public static class VHRetry extends RecyclerView.ViewHolder {
        private OnItemClickListener clickListener;

        @OnClick(R.id.btn_retry) public void itemClick() {
            clickListener.onItemClick(getAdapterPosition());
        }

        public VHRetry(View view, OnItemClickListener clickListener) {
            super(view);
            ButterKnife.bind(this, view);
            this.clickListener = clickListener;
        }
    }

    protected OnItemClickListener itemClickListener = new OnItemClickListener() {
        @Override
        public void onItemClick(int position) {
            itemClick(position);
        }
    };

    protected OnItemClickListener retryClickListener = new OnItemClickListener() {
        @Override
        public void onItemClick(int position) {
            setFooterType(FOOTER_PROGRESS);
        }
    };

    public abstract void loadData();

    public abstract void itemClick(int position);
}
