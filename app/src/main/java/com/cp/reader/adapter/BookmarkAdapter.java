package com.cp.reader.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.cp.reader.R;
import com.cp.reader.listener.OnItemClickListener;
import com.cp.reader.listener.OnItemLongClickListener;
import com.cp.reader.model.Bookmark;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.List;
import java.util.Locale;

public abstract class BookmarkAdapter extends RecyclerView.Adapter<BookmarkAdapter.ViewHolder> {
    private Context context;
    private List<Bookmark> listBookmark;

    private DateTimeFormatter dateFormatter;
    private Locale indonesia;

    public BookmarkAdapter(Context context, List<Bookmark> listBookmark) {
        this.context = context;
        this.listBookmark = listBookmark;

        this.dateFormatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
        this.indonesia = new Locale("in_ID");
    }

    @Override
    public int getItemCount() {
        return listBookmark.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        View view = inflater.inflate(R.layout.row_list_post, parent, false);
        return new ViewHolder(view, itemClickListener, itemLongClickListener);
    }

    public void replaceData(List<Bookmark> listBookmark) {
        this.listBookmark = listBookmark;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // Atur background
        holder.cardView.setCardBackgroundColor(Color.WHITE);

        // Ambil bookmark
        Bookmark bookmark = listBookmark.get(position);

        // Tampilkan tanggal
        LocalDateTime dateTime = LocalDateTime.parse(bookmark.getDate(), dateFormatter);
        holder.txtPostDate.setText(dateTime.toString("dd MMMM yyyy, HH:mm:ss", indonesia));

        // Tampilkan post title
        holder.txtPostTitle.setText(Html.fromHtml(bookmark.getTitle()));

        // Load image
        Glide.with(context)
                .load(bookmark.getThumbnailMedium())
                .placeholder(R.color.background)
                .centerCrop()
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.imgThumbnail);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private OnItemClickListener itemClickListener;
        private OnItemLongClickListener itemLongClickListener;

        @Bind(R.id.card_view) CardView cardView;
        @Bind(R.id.img_thumbnail) ImageView imgThumbnail;
        @Bind(R.id.txt_post_date) TextView txtPostDate;
        @Bind(R.id.txt_post_title) TextView txtPostTitle;

        @OnClick(R.id.item_post) public void itemClick() {
            itemClickListener.onItemClick(getAdapterPosition());
        }

        @OnLongClick(R.id.item_post) public boolean itemLongClick() {
            return itemLongClickListener.onItemLongClick(getAdapterPosition());
        }

        public ViewHolder(View view, OnItemClickListener itemClickListener, OnItemLongClickListener itemLongClickListener) {
            super(view);
            ButterKnife.bind(this, view);
            this.itemClickListener = itemClickListener;
            this.itemLongClickListener = itemLongClickListener;
        }
    }

    protected OnItemClickListener itemClickListener = new OnItemClickListener() {
        @Override
        public void onItemClick(int position) {
            itemClick(position);
        }
    };

    protected OnItemLongClickListener itemLongClickListener = new OnItemLongClickListener() {
        @Override
        public boolean onItemLongClick(int position) {
            return itemLongClick(position);
        }
    };

    public abstract void itemClick(int position);

    public abstract boolean itemLongClick(int position);
}
