package com.cp.reader.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.cp.reader.R;
import com.cp.reader.model.Tag;

import java.util.List;

public class DialogTagAdapter extends ArrayAdapter<Tag.Result> {
    private List<Tag.Result> listTag;

    public DialogTagAdapter(Context context, List<Tag.Result> listTag) {
        super(context, R.layout.row_list_category, listTag);
        this.listTag = listTag;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder holder;

        if (view != null) holder = (ViewHolder) view.getTag();
        else {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            view = inflater.inflate(R.layout.row_list_tag_dialog, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }

        holder.txtTag.setText(listTag.get(position).name);

        return view;
    }

    public static class ViewHolder {
        @Bind(R.id.txt_tag) TextView txtTag;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
