package com.cp.reader.adapter;

import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.cp.reader.R;
import com.cp.reader.listener.OnItemClickListener;
import com.cp.reader.model.Category;

import java.util.List;

public abstract class CategoryAdapter extends BaseAdapter {
    private List<Category.Result> listCategory;

    public CategoryAdapter(List<Category.Result> listCategory) {
        this.listCategory = listCategory;
    }

    @Override
    public int getItemCount() {
        if (footerType != NO_FOOTER) return listCategory.size() + 1;
        else return listCategory.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        if (viewType == ITEM) {
            View v = inflater.inflate(R.layout.row_list_category, parent, false);
            return new VHItem(v, itemClickListener);
        } else {
            return super.onCreateViewHolder(parent, viewType);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int type = getItemViewType(position);

        if (type == RETRY) return;
        if (type == PROGRESS) loadData();
        else {
            VHItem vhItem = (VHItem) holder;
            Category.Result category = listCategory.get(position);
            vhItem.txtCategory.setText(Html.fromHtml(category.name));
        }
    }

    public void replaceData(List<Category.Result> listCategory) {
        this.listCategory = listCategory;
        notifyDataSetChanged();
    }

    public static class VHItem extends RecyclerView.ViewHolder {
        private OnItemClickListener itemClickListener;

        @Bind(R.id.item_category) TextView txtCategory;

        @OnClick(R.id.item_category) public void itemClick() {
            itemClickListener.onItemClick(getAdapterPosition());
        }

        public VHItem(View view, OnItemClickListener itemClickListener) {
            super(view);
            ButterKnife.bind(this, view);
            this.itemClickListener = itemClickListener;
        }
    }
}
