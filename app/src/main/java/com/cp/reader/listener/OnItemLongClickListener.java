package com.cp.reader.listener;

public interface OnItemLongClickListener {
    boolean onItemLongClick(int position);
}