package com.cp.reader.listener;

public interface OnItemClickListener {
    void onItemClick(int position);
}