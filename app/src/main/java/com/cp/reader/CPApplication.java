package com.cp.reader;

import android.app.Application;
import com.cp.reader.web.ApiService;
import com.cp.reader.web.RestClient;
import com.squareup.otto.Bus;
import io.realm.Realm;

public class CPApplication extends Application {
    private ApiService apiService;
    private Bus eventBus;
    private Realm realm;

    @Override
    public void onCreate() {
        super.onCreate();
        getEventBus().register(this);
    }

    public ApiService getApi() {
        if (apiService == null) apiService = RestClient.getService();
        return apiService;
    }

    public Bus getEventBus() {
        if (eventBus == null) eventBus = new com.squareup.otto.Bus();
        return eventBus;
    }

    public Realm getRealm() {
        if (realm == null) realm = Realm.getInstance(this);
        return realm;
    }
}
