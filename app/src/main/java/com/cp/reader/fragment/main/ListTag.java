package com.cp.reader.fragment.main;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.cp.reader.R;
import com.cp.reader.activity.MainActivity;
import com.cp.reader.adapter.TagAdapter;
import com.cp.reader.model.Tag;
import com.cp.reader.web.ApiService;
import com.squareup.otto.Subscribe;
import icepick.Icepick;
import icepick.State;
import org.parceler.Parcels;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import java.util.ArrayList;
import java.util.List;

public class ListTag extends BaseFragment {
    @Bind(R.id.progress_bar) ProgressBar progressBar;
    @Bind(R.id.recycler_view) RecyclerView recyclerView;

    private ApiService api;
    private TagAdapter adapter;

    @State int currentPage = 1;
    @State boolean isLoading = false;
    @State int footerType = TagAdapter.NO_FOOTER;
    private List<Tag.Result> listTag = new ArrayList<Tag.Result>();

    private static final String ARG_LIST_TAG = "ARG-LIST-TAG";

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Icepick.saveInstanceState(this, outState);
        outState.putParcelable(ARG_LIST_TAG, Parcels.wrap(listTag));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate and inject view
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        ButterKnife.bind(this, view);

        // Ambil api service
        api = application.getApi();

        // Buat layout manager untuk RecyclerView
        LinearLayoutManager layoutManager = new LinearLayoutManager(activity);

        // Ambil data dari saved instance kalau ada
        if (savedInstanceState != null) {
            Icepick.restoreInstanceState(this, savedInstanceState);
            listTag = Parcels.unwrap(savedInstanceState.getParcelable(ARG_LIST_TAG));
        }

        // Pastikan bahwa list tag tidak null
        if (listTag == null) listTag = new ArrayList<Tag.Result>();

        // Buat adapter
        adapter = new TagAdapter(listTag) {
            @Override
            public void loadData() {
                loadTag();
            }

            @Override
            public void itemClick(int position) {
                isLoading = false;
                Tag.Result tag = listTag.get(position);
                activity.startListFragment(
                        "#" + Html.fromHtml(tag.name).toString(),
                        tag.slug,
                        MainActivity.TAG
                );
            }
        };

        // Atur footer adapter
        adapter.setFooterType(footerType);

        // Atur recycler view
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        // Load daftar tag kalau perlu
        if (listTag.isEmpty()) {
            if (isLoading) progressBar.setVisibility(View.VISIBLE);
            else loadTag();
        }

        return view;
    }

    private void loadTag() {
        // Kalau masih loading, hentikan proses
        if (isLoading) return;

        // Tandai bahwa proses loading dimulai
        isLoading = true;

        // Kalau belum ada tag yang terdaftar, tampilkan progress
        if (listTag.isEmpty()) progressBar.setVisibility(View.VISIBLE);

        // Ambil data dari internet
        api.getPopularTag(currentPage, new Callback<Tag>() {
            @Override
            public void success(Tag result, Response response) {
                application.getEventBus().post(result);
            }

            @Override
            public void failure(RetrofitError error) {
                application.getEventBus().post(error);
            }
        });
    }

    @Subscribe
    public void getListSuccess(Tag tag) {
        // Tandai bahwa proses loading selesai
        isLoading = false;

        // Sembunyikan progress bar
        progressBar.setVisibility(View.GONE);

        // Kalau kode hasil API bukan 200,
        // berarti tidak ada lagi data yang bisa diambil
        if (tag.code != 200) {
            footerType = TagAdapter.NO_FOOTER;
            adapter.setFooterType(footerType);
            return;
        }

        // Masukkan daftar baru ke dalam list
        listTag.addAll(tag.result);

        // Tampilkan ke dalam list
        adapter.replaceData(listTag);

        // Inkremen nomor halaman
        currentPage++;

        // Aktifkan progress pada list
        footerType = TagAdapter.FOOTER_PROGRESS;
        adapter.setFooterType(footerType);
    }

    @Subscribe
    public void getListFailed(RetrofitError error) {
        // Tandai bahwa proses loading selesai
        isLoading = false;

        // Sembunyikan progress bar
        progressBar.setVisibility(View.GONE);

        // Atur footer list
        if (listTag.isEmpty()) {
            footerType = TagAdapter.NO_FOOTER;
            Toast.makeText(activity, R.string.fail_to_fetch_data, Toast.LENGTH_SHORT).show();
        } else footerType = TagAdapter.FOOTER_RETRY;
        adapter.setFooterType(footerType);
    }

    @Override
    public void resetData() {
        // Cek kalau masih loading
        if (isLoading) {
            Toast.makeText(activity, R.string.is_fetching_data, Toast.LENGTH_SHORT).show();
            return;
        }

        // Ubah parameter ke nilai default
        listTag = new ArrayList<Tag.Result>();
        footerType = TagAdapter.NO_FOOTER;
        currentPage = 1;

        // Reset adapter
        adapter.replaceData(listTag);
        adapter.setFooterType(footerType);

        // Load post baru
        loadTag();
    }
}
