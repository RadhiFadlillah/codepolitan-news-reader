package com.cp.reader.fragment.main;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.*;
import android.widget.ProgressBar;
import android.widget.Toast;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.cp.reader.R;
import com.cp.reader.activity.DetailActivity;
import com.cp.reader.adapter.PostAdapter;
import com.cp.reader.model.Post;
import com.cp.reader.web.ApiService;
import com.squareup.otto.Subscribe;
import icepick.Icepick;
import icepick.State;
import org.parceler.Parcels;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import java.util.ArrayList;
import java.util.List;

public class ListPost extends BaseFragment {
    @Bind(R.id.progress_bar) ProgressBar progressBar;
    @Bind(R.id.recycler_view) RecyclerView recyclerView;

    private ApiService api;
    private PostAdapter adapter;

    private String title, slug;
    private int listType;
    private boolean hasHeader;

    @State int currentPage = 1;
    @State boolean isLoading = false;
    @State int footerType = PostAdapter.NO_FOOTER;
    private List<Post.Result> listPost = new ArrayList<Post.Result>();

    private static final int COLUMN_WIDTH_DP = 350;

    private static final String LIST_TITLE = "LIST-TITLE";
    private static final String LIST_SLUG = "LIST-SLUG";
    private static final String LIST_TYPE = "LIST-TYPE";
    private static final String LIST_HAS_HEADER = "LIST-HAS-HEADER";
    private static final String ARG_LIST_POST = "ARG-LIST-POST";

    public static final int LATEST = 0;
    public static final int BY_CATEGORY = 1;
    public static final int BY_TAG = 2;
    public static final int BY_TYPE = 3;
    public static final int SEARCH = 4;

    public static ListPost newInstance(String title, String slug, int listType, boolean hasHeader) {
        Bundle args = new Bundle();
        args.putString(LIST_TITLE, title);
        args.putString(LIST_SLUG, slug);
        args.putInt(LIST_TYPE, listType);
        args.putBoolean(LIST_HAS_HEADER, hasHeader);

        ListPost listPost = new ListPost();
        listPost.setArguments(args);

        return listPost;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Icepick.saveInstanceState(this, outState);
        outState.putParcelable(ARG_LIST_POST, Parcels.wrap(listPost));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate and inject view
        View view = inflater.inflate(R.layout.fragment_list_post, container, false);
        ButterKnife.bind(this, view);

        // Ambil argumen
        if (getArguments() != null) {
            title = getArguments().getString(LIST_TITLE, null);
            slug = getArguments().getString(LIST_SLUG, null);
            listType = getArguments().getInt(LIST_TYPE, LATEST);
            hasHeader = getArguments().getBoolean(LIST_HAS_HEADER, true);
        } else {
            listType = LATEST;
            hasHeader = true;
        }

        // Jika bukan daftar post terbaru,
        // maka kunci navigation drawer dan sembunyikan tab layout
        // serta atur menu di toolbar
        if (listType != LATEST) {
            activity.lockNavDrawer(true);
            activity.getTabLayout().setVisibility(View.GONE);

            Toolbar toolbar = activity.getToolbar();
            toolbar.setTitle(title);

            toolbar.setNavigationIcon(R.drawable.ic_back);
            toolbar.setNavigationOnClickListener(navigationOnClickListener);

            toolbar.getMenu().clear();
            toolbar.inflateMenu(R.menu.list);
            toolbar.setOnMenuItemClickListener(menuItemClickListener);
        }

        // Ambil api service
        api = application.getApi();

        // Ambil window manager
        WindowManager wm = (WindowManager) activity.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();

        // Hitung ukuran layar
        Point size = new Point();
        display.getSize(size);

        // Ubah lebar kolom dari dp ke px
        DisplayMetrics displayMetrics = activity.getResources().getDisplayMetrics();
        int columnWidth = (int)((COLUMN_WIDTH_DP * displayMetrics.density) + 0.5);

        // Hitung jumlah kolom
        int columnSize = (int)((1.0 * size.x / columnWidth) + 0.5);

        // Buat layout manager untuk RecyclerView
        StaggeredGridLayoutManager layoutManager;
        layoutManager = new StaggeredGridLayoutManager(columnSize, StaggeredGridLayoutManager.VERTICAL);

        // Ambil data dari saved instance kalau ada
        if (savedInstanceState != null) {
            Icepick.restoreInstanceState(this, savedInstanceState);
            listPost = Parcels.unwrap(savedInstanceState.getParcelable(ARG_LIST_POST));
        }

        // Pastikan bahwa list post tidak null
        if (listPost == null) listPost = new ArrayList<Post.Result>();

        // Buat adapter
        adapter = new PostAdapter(activity, listPost) {
            @Override
            public void loadData() {
                loadPost();
            }

            @Override
            public void itemClick(int position) {
                isLoading = false;

                Intent intent = new Intent(activity, DetailActivity.class);
                intent.putExtra(DetailActivity.POST_POSITION, position);
                intent.putExtra(DetailActivity.LIST_POST, Parcels.wrap(listPost));
                intent.putExtra(DetailActivity.HAS_HEADER, hasHeader);
                startActivity(intent);
            }
        };

        // Atur footer adapter
        adapter.setFooterType(footerType);

        // Atur recycler view
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        // Load daftar post kalau perlu
        if (listPost.isEmpty()) {
            if (isLoading) progressBar.setVisibility(View.VISIBLE);
            else loadPost();
        }

        return view;
    }

    public void loadPost() {
        // Kalau masih loading, hentikan proses
        if (isLoading) return;

        // Tandai bahwa proses loading dimulai
        isLoading = true;

        // Kalau belum ada post yang terdaftar, tampilkan progress
        if (listPost.isEmpty()) progressBar.setVisibility(View.VISIBLE);

        // Ambil data dari internet
        switch (listType) {
            case BY_CATEGORY: api.getPostByCategory(slug, currentPage, callbackPost); break;
            case BY_TAG: api.getPostByTag(slug, currentPage, callbackPost); break;
            case BY_TYPE: api.getPostByType(slug, currentPage, callbackPost); break;
            case SEARCH: api.searchPost(slug, currentPage, callbackPost); break;
            default: api.getLatestPost(currentPage, callbackPost);
        }
    }

    @Override
    protected void navigationOnClick() {
        if (activity.getSupportFragmentManager().getBackStackEntryCount() > 0)
            activity.getSupportFragmentManager().popBackStack();
        else activity.finish();
    }

    private Toolbar.OnMenuItemClickListener menuItemClickListener = new Toolbar.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(MenuItem item) {
            if (item.getItemId() != R.id.menu_refresh) return false;
            resetData(); return true;
        }
    };

    private Callback<Post> callbackPost = new Callback<Post>() {
        @Override
        public void success(Post result, Response response) {
            application.getEventBus().post(new PostAndIdentifier(result, slug + String.valueOf(currentPage)));
        }

        @Override
        public void failure(RetrofitError error) {
            application.getEventBus().post(error);
        }
    };

    @Subscribe
    public void getListSuccess(PostAndIdentifier postAndId) {
        // Buat identifier
        String identifier = slug + String.valueOf(currentPage);

        // Pastikan ini list post yang diinginkan
        if (!identifier.equals(postAndId.identifier)) return;

        // Tandai bahwa proses loading selesai
        isLoading = false;

        // Sembunyikan progress bar
        progressBar.setVisibility(View.GONE);

        // Ambil post
        Post post = postAndId.post;

        // Kalau kode hasil API bukan 200,
        // berarti tidak ada lagi data yang bisa diambil
        if (post.code != 200) {
            footerType = PostAdapter.NO_FOOTER;
            adapter.setFooterType(footerType);

            // Kalau list nya memang kosong tampilkan pesan
            if (listPost.isEmpty()) {
                Toast.makeText(activity, R.string.data_not_found, Toast.LENGTH_SHORT).show();
            }

            return;
        }

        // Masukkan daftar baru ke dalam list
        listPost.addAll(post.result);

        // Tampilkan ke dalam list
        adapter.replaceData(listPost);

        // Inkremen nomor halaman
        currentPage++;

        // Aktifkan progress pada list
        footerType = PostAdapter.FOOTER_PROGRESS;
        adapter.setFooterType(footerType);
    }

    @Subscribe
    public void getListFailed(RetrofitError error) {
        // Tandai bahwa proses loading selesai
        isLoading = false;

        // Sembunyikan progress bar
        progressBar.setVisibility(View.GONE);

        // Atur footer list
        if (listPost.isEmpty()) {
            footerType = PostAdapter.NO_FOOTER;
            Toast.makeText(activity, R.string.fail_to_fetch_data, Toast.LENGTH_SHORT).show();
        } else footerType = PostAdapter.FOOTER_RETRY;
        adapter.setFooterType(footerType);
    }

    @Override
    public void resetData() {
        // Cek kalau masih loading
        if (isLoading) {
            Toast.makeText(activity, R.string.is_fetching_data, Toast.LENGTH_SHORT).show();
            return;
        }

        // Ubah parameter ke nilai default
        listPost = new ArrayList<Post.Result>();
        footerType = PostAdapter.NO_FOOTER;
        currentPage = 1;

        // Reset adapter
        adapter.replaceData(listPost);
        adapter.setFooterType(footerType);

        // Load post baru
        loadPost();
    }

    public static class PostAndIdentifier {
        public Post post;
        public String identifier;

        public PostAndIdentifier(Post post, String identifier) {
            this.post = post;
            this.identifier = identifier;
        }
    }
}
