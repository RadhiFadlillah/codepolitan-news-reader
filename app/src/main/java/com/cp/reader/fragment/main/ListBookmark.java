package com.cp.reader.fragment.main;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.*;
import android.widget.Toast;
import butterknife.Bind;
import butterknife.ButterKnife;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.cp.reader.R;
import com.cp.reader.activity.DetailActivity;
import com.cp.reader.adapter.BookmarkAdapter;
import com.cp.reader.model.Bookmark;
import io.realm.Realm;

import java.util.ArrayList;
import java.util.List;

public class ListBookmark extends BaseFragment {
    @Bind(R.id.recycler_view) RecyclerView recyclerView;

    private Realm realm;
    private BookmarkAdapter adapter;
    private List<Bookmark> listBookmark = new ArrayList<Bookmark>();

    private static final int COLUMN_WIDTH_DP = 350;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate and inject view
        View view = inflater.inflate(R.layout.fragment_list_post, container, false);
        ButterKnife.bind(this, view);

        // Kunci navigation drawer
        activity.lockNavDrawer(true);

        // Ambil realm
        realm = application.getRealm();

        // Atur toolbar
        Toolbar toolbar = activity.getToolbar();
        toolbar.setTitle(R.string.bookmark);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(navigationOnClickListener);
        toolbar.getMenu().clear();

        // Sembunyikan tab layout
        activity.getTabLayout().setVisibility(View.GONE);

        // Ambil data bookmark
        listBookmark = realm.where(Bookmark.class).findAllSorted("id", false);

        // Kalau tidak ada bookmark, tampilkan pesan
        if (listBookmark.isEmpty()) {
            Toast.makeText(activity, R.string.no_bookmark_available, Toast.LENGTH_SHORT).show();
            return view;
        }

        // Buat adapter
        adapter = new BookmarkAdapter(activity, listBookmark) {
            @Override
            public void itemClick(int position) {
                Intent intent = new Intent(activity, DetailActivity.class);
                intent.putExtra(DetailActivity.POST_POSITION, position);
                startActivity(intent);
            }

            @Override
            public boolean itemLongClick(final int position) {
                final Bookmark bookmark = listBookmark.get(position);
                new MaterialDialog.Builder(activity)
                        .title(R.string.title_remove_bookmark)
                        .content(R.string.content_remove_bookmark)
                        .positiveText(R.string.yes)
                        .negativeText(R.string.no)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                realm.beginTransaction();
                                bookmark.removeFromRealm();
                                realm.commitTransaction();

                                Toast.makeText(activity, R.string.bookmark_removed, Toast.LENGTH_SHORT).show();
                                adapter.replaceData(listBookmark);
                            }
                        }).show();
                return true;
            }
        };

        // Ambil window manager
        WindowManager wm = (WindowManager) activity.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();

        // Hitung ukuran layar
        Point size = new Point();
        display.getSize(size);

        // Ubah lebar kolom dari dp ke px
        DisplayMetrics displayMetrics = activity.getResources().getDisplayMetrics();
        int columnWidth = (int)((COLUMN_WIDTH_DP * displayMetrics.density) + 0.5);

        // Hitung jumlah kolom
        int columnSize = (int)((1.0 * size.x / columnWidth) + 0.5);

        // Buat layout manager untuk RecyclerView
        StaggeredGridLayoutManager layoutManager;
        layoutManager = new StaggeredGridLayoutManager(columnSize, StaggeredGridLayoutManager.VERTICAL);

        // Atur recycler view
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        return view;
    }

    @Override
    protected void navigationOnClick() {
        activity.finish();
    }
}
