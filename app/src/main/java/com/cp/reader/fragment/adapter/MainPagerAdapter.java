package com.cp.reader.fragment.adapter;

import android.content.Context;
import android.graphics.Point;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.DisplayMetrics;
import android.util.SparseArray;
import android.view.Display;
import android.view.ViewGroup;
import android.view.WindowManager;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class MainPagerAdapter extends FragmentPagerAdapter {
    SparseArray<WeakReference<Fragment>> registeredFragment = new SparseArray<WeakReference<Fragment>>();
    private ArrayList<Fragment> listFragment;
    private ArrayList<String> listTitle;
    private int screenWidth;
    private int sidePageWidth;

    private static final int DP_SIDE_PAGE_WIDTH = 270;

    public MainPagerAdapter(Context context, FragmentManager fragmentManager, ArrayList<Fragment> listFragment, ArrayList<String> listTitle) {
        super(fragmentManager);
        this.listFragment = listFragment;
        this.listTitle = listTitle;

        // Ambil window manager
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();

        // Hitung lebar layar
        Point size = new Point();
        display.getSize(size);
        screenWidth = size.x;

        // Ubah lebar kolom samping dari dp ke px
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        sidePageWidth = (int)((DP_SIDE_PAGE_WIDTH * displayMetrics.density) + 0.5);
    }

    @Override
    public Fragment getItem(int position) {
        return listFragment.get(position);
    }

    @Override
    public int getCount() {
        return listFragment.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return listTitle.get(position);
    }

    @Override
    public float getPageWidth(int position) {
        if (position == 1) return 1f;
        else return 1f * sidePageWidth / screenWidth;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        registeredFragment.put(position, new WeakReference<Fragment>(fragment));
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        registeredFragment.remove(position);
        super.destroyItem(container, position, object);
    }

    public Fragment getRegisteredFragment(int position) {
        return registeredFragment.get(position).get();
    }
}
