package com.cp.reader.fragment.main;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.View;
import butterknife.ButterKnife;
import com.cp.reader.CPApplication;
import com.cp.reader.activity.MainActivity;

public class BaseFragment extends Fragment {
    protected MainActivity activity;
    protected CPApplication application;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.activity = (MainActivity) getActivity();
        this.application = (CPApplication) activity.getApplication();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        application.getEventBus().unregister(this);
    }

    @Override
    public void onResume() {
        application.getEventBus().register(this);
        super.onResume();
    }

    protected View.OnClickListener navigationOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            navigationOnClick();
        }
    };

    protected void navigationOnClick() {}

    public void resetData() {}
}
