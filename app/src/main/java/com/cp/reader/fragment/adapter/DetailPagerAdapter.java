package com.cp.reader.fragment.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;
import com.cp.reader.fragment.detail.FragmentDetail;
import com.cp.reader.model.Bookmark;
import com.cp.reader.model.Post;

import java.lang.ref.WeakReference;
import java.util.List;

public class DetailPagerAdapter extends FragmentPagerAdapter {
    SparseArray<WeakReference<Fragment>> registeredFragment = new SparseArray<WeakReference<Fragment>>();
    private List<Post.Result> listPost;
    private List<Bookmark> listBookmark;
    private boolean hasHeader;

    public DetailPagerAdapter(FragmentManager fragmentManager, boolean hasHeader) {
        super(fragmentManager);
        this.hasHeader = hasHeader;
    }

    public void setPost(List<Post.Result> listPost) {
        this.listPost = listPost;
    }

    public void setBookmark(List<Bookmark> listBookmark) {
        this.listBookmark = listBookmark;
    }

    @Override
    public Fragment getItem(int position) {
        if (listPost != null) return FragmentDetail.newPostInstance(listPost.get(position), hasHeader);
        else {
            Bookmark bookmark = listBookmark.get(position);
            return FragmentDetail.newBookmarkInstance(bookmark.getId(), bookmark.isHasHeader());
        }
    }

    @Override
    public int getCount() {
        if (listPost != null) return listPost.size();
        else return listBookmark.size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        registeredFragment.put(position, new WeakReference<Fragment>(fragment));
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        registeredFragment.remove(position);
        super.destroyItem(container, position, object);
    }

    public Fragment getRegisteredFragment(int position) {
        return registeredFragment.get(position).get();
    }
}