package com.cp.reader.fragment.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.afollestad.materialdialogs.MaterialDialog;
import com.cp.reader.R;
import com.cp.reader.activity.MainActivity;
import com.cp.reader.fragment.adapter.MainPagerAdapter;

import java.util.ArrayList;

public class FragmentHome extends BaseFragment {
    @Bind(R.id.view_pager) ViewPager viewPager;

    private MainPagerAdapter adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Buat list judul
        ArrayList<String> listTitle = new ArrayList<String>();
        listTitle.add("Kategori");
        listTitle.add("Home");
        listTitle.add("Tag");

        // Buat list fragment
        ArrayList<Fragment> listFragment = new ArrayList<Fragment>();
        listFragment.add(new ListCategory());
        listFragment.add(new ListPost());
        listFragment.add(new ListTag());

        // Buat adapter
        adapter = new MainPagerAdapter(getContext(), getChildFragmentManager(), listFragment, listTitle);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate and inject view
        View view = inflater.inflate(R.layout.fragment_pager, container, false);
        ButterKnife.bind(this, view);

        // Buka kunci nav drawer
        activity.lockNavDrawer(false);

        // Ambil toolbar dari activity
        Toolbar toolbar = activity.getToolbar();

        // Atur judul toolbar
        toolbar.setTitle(R.string.codepolitan);

        // Atur navigasi
        toolbar.setNavigationIcon(R.drawable.ic_menu);
        toolbar.setNavigationOnClickListener(navigationOnClickListener);

        // Atur menu di toolbar
        toolbar.getMenu().clear();
        toolbar.inflateMenu(R.menu.home);
        toolbar.setOnMenuItemClickListener(menuItemClickListener);

        // Tampilkan viewpager
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(1);
        viewPager.setOffscreenPageLimit(3);

        // Ambil dan tampilkan tab layout
        TabLayout tabs = activity.getTabLayout();
        tabs.setVisibility(View.VISIBLE);
        tabs.setupWithViewPager(viewPager);

        return view;
    }

    @Override
    protected void navigationOnClick() {
        activity.showNavDrawer();
    }

    private Toolbar.OnMenuItemClickListener menuItemClickListener = new Toolbar.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(MenuItem item) {
            switch (item.getItemId()) {
                case R.id.menu_refresh:
                    int currentItem = viewPager.getCurrentItem();
                    BaseFragment fragment = (BaseFragment) adapter.getRegisteredFragment(currentItem);
                    fragment.resetData();
                    return true;

                case R.id.menu_search:
                    new MaterialDialog.Builder(activity)
                            .title(R.string.title_search)
                            .content(R.string.content_search)
                            .inputType(InputType.TYPE_CLASS_TEXT)
                            .input(R.string.hint_search, R.string.null_string, false, searchCallback)
                            .show();
                    return true;
            }

            return false;
        }
    };

    private MaterialDialog.InputCallback searchCallback = new MaterialDialog.InputCallback() {
        @Override
        public void onInput(@NonNull MaterialDialog materialDialog, CharSequence charSequence) {
            String keyword = charSequence.toString();
            activity.startListFragment("\"" + keyword + "\"", keyword, MainActivity.SEARCH);
        }
    };
}
