package com.cp.reader.fragment.detail;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.afollestad.materialdialogs.MaterialDialog;
import com.cp.reader.CPApplication;
import com.cp.reader.R;
import com.cp.reader.activity.DetailActivity;
import com.cp.reader.activity.MainActivity;
import com.cp.reader.adapter.DialogTagAdapter;
import com.cp.reader.model.Bookmark;
import com.cp.reader.model.Post;
import com.cp.reader.model.PostDetail;
import com.cp.reader.model.Tag;
import com.cp.reader.web.ApiService;
import com.squareup.otto.Subscribe;
import icepick.Icepick;
import icepick.State;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.parceler.Parcels;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import java.util.Locale;

public class FragmentDetail extends Fragment {
    @Bind(R.id.card_view) CardView cardView;
    @Bind(R.id.txt_post_title) TextView txtPostTitle;
    @Bind(R.id.txt_post_category) TextView txtPostCategory;
    @Bind(R.id.txt_post_date) TextView txtPostDate;
    @Bind(R.id.txt_post_tag) TextView txtPostTag;
    @Bind(R.id.webview) WebView webView;
    @Bind(R.id.progress_bar) ProgressBar progressBar;

    private ApiService api;

    private DetailActivity activity;
    private CPApplication application;

    private Post.Result post;
    private Bookmark bookmark;
    private PostDetail.Result postDetail;
    private boolean hasHeader;

    @State boolean isLoading = false;

    private static final String SELECTED_POST = "SELECTED-POST";
    private static final String BOOKMARK_ID = "BOOKMARK-ID";
    private static final String HAS_HEADER = "HAS-HEADER";
    private static final String ARG_POST_DETAIL = "POST-DETAIL";

    public static FragmentDetail newPostInstance(Post.Result post, boolean hasHeader) {
        Bundle args = new Bundle();
        args.putParcelable(SELECTED_POST, Parcels.wrap(post));
        args.putBoolean(HAS_HEADER, hasHeader);

        FragmentDetail fragmentDetail = new FragmentDetail();
        fragmentDetail.setArguments(args);

        return fragmentDetail;
    }

    public static FragmentDetail newBookmarkInstance(int bookmarkId, boolean hasHeader) {
        Bundle args = new Bundle();
        args.putInt(BOOKMARK_ID, bookmarkId);
        args.putBoolean(HAS_HEADER, hasHeader);

        FragmentDetail fragmentDetail = new FragmentDetail();
        fragmentDetail.setArguments(args);

        return fragmentDetail;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.activity = (DetailActivity) getActivity();
        this.application = (CPApplication) activity.getApplication();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        application.getEventBus().unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        application.getEventBus().register(this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Icepick.saveInstanceState(this, outState);
        outState.putParcelable(ARG_POST_DETAIL, Parcels.wrap(postDetail));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate and inject view
        View view = inflater.inflate(R.layout.fragment_detail, container, false);
        ButterKnife.bind(this, view);

        // Ambil API
        api = application.getApi();

        // Ambil argumen
        Parcelable parcelPost = getArguments().getParcelable(SELECTED_POST);
        int idBookmark = getArguments().getInt(BOOKMARK_ID);
        hasHeader = getArguments().getBoolean(HAS_HEADER, true);

        // Unparse parcelable atau ambil bookmark
        if (parcelPost != null) post = Parcels.unwrap(parcelPost);
        else bookmark = application.getRealm()
                .where(Bookmark.class)
                .equalTo("id", idBookmark)
                .findFirst();

        // Atur judul dan tanggal
        setPostTitle();

        // Atur latar card view
        cardView.setCardBackgroundColor(Color.WHITE);

        // Atur webview
        webView.setScrollContainer(false);
        webView.setWebViewClient(new CPWebClient());

        // Ambil post detail
        if (savedInstanceState == null) loadData();
        else {
            Icepick.restoreInstanceState(this, savedInstanceState);
            postDetail = Parcels.unwrap(savedInstanceState.getParcelable(ARG_POST_DETAIL));
            if (postDetail != null) showPostDetail(postDetail);
        }

        return view;
    }

    private void setPostTitle() {
        // Ambil judul dan tanggal dari post atau bookmark
        String title, date;
        if (post != null) {
            title = post.title;
            date = post.date;
        } else {
            title = bookmark.getTitle();
            date = bookmark.getDate();
        }

        // Tampilkan judul
        txtPostTitle.setText(Html.fromHtml(title));

        // Tampilkan tanggal
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime dateTime = LocalDateTime.parse(date, formatter);
        txtPostDate.setText(dateTime.toString("dd MMMM yyyy, HH:mm:ss", new Locale("in_ID")));
    }

    private void loadData() {
        // Tandai bahwa proses dimulai
        isLoading = true;
        progressBar.setVisibility(View.VISIBLE);

        // Ambil id post
        int id; if (post != null) id = post.id;
        else id = bookmark.getId();

        api.getPostDetail(id, new Callback<PostDetail>() {
            @Override
            public void success(PostDetail postDetail, Response response) {
                application.getEventBus().post(postDetail);
            }

            @Override
            public void failure(RetrofitError error) {
                application.getEventBus().post(error);
            }
        });
    }

    public void resetData() {
        if (isLoading) {
            Toast.makeText(activity, R.string.is_fetching_data, Toast.LENGTH_SHORT).show();
            return;
        }

        postDetail = null;

        txtPostCategory.setVisibility(View.GONE);
        txtPostTag.setVisibility(View.GONE);
        webView.setVisibility(View.GONE);

        loadData();
    }

    @Subscribe
    public void getDetailSuccess(PostDetail detail) {
        // Ambil id post
        int id; if (post != null) id = post.id;
        else id = bookmark.getId();

        // Pastikan ini post yang diinginkan
        if (detail.result.id != id) return;

        isLoading = false;
        postDetail = detail.result;
        showPostDetail(postDetail);
    }

    @Subscribe
    public void getDetailFailed(RetrofitError error) {
        isLoading = false;

        // Sembunyikan progress bar
        progressBar.setVisibility(View.GONE);

        // Tampilkan pesan
        Toast.makeText(activity,R.string.fail_to_fetch_data,Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.txt_post_category)
    public void onCategoryClick() {
        String categoryName = postDetail.category.get(0).name;
        String categorySlug = postDetail.category.get(0).slug;

        Intent intent = new Intent(activity, MainActivity.class);
        intent.putExtra(MainActivity.LIST_TITLE, Html.fromHtml(categoryName).toString());
        intent.putExtra(MainActivity.LIST_SLUG, categorySlug);
        intent.putExtra(MainActivity.LIST_TYPE, MainActivity.CATEGORY);
        activity.startActivity(intent);
    }

    @OnClick(R.id.txt_post_tag)
    public void onTagClick() {
        new MaterialDialog.Builder(activity)
                .title(R.string.tag_list)
                .adapter(new DialogTagAdapter(activity, postDetail.tags), dialogTagCallback)
                .show();
    }

    private class CPWebClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
            return true;
        }
    }

    private void showPostDetail(PostDetail.Result postDetail) {
        // Sembunyikan progress bar
        progressBar.setVisibility(View.GONE);

        // Tampilkan kategori
        if (!postDetail.category.isEmpty()) {
            String category = postDetail.category.get(0).name;
            txtPostCategory.setText(Html.fromHtml(category));
            txtPostCategory.setVisibility(View.VISIBLE);
        }

        // Tampilkan tag
        String tags = "";
        if (!postDetail.tags.isEmpty()) for (Tag.Result tag : postDetail.tags) tags += "#" + tag.name + "  ";
        if (!tags.isEmpty()) {
            txtPostTag.setText(tags);
            txtPostTag.setVisibility(View.VISIBLE);
        }

        // Buat string untuk konten HTML
        String contentHtml;
        if (!hasHeader) {
            String imgUrl = postDetail.thumbnailSmall.replace("-150x150", "");
            contentHtml = "<img src=\""+ imgUrl + "\">" + postDetail.content;
        } else contentHtml = postDetail.content;

        // Perbaiki HTML
        contentHtml = fixPostContent(contentHtml);

        // Tampilkan content
        webView.setVisibility(View.VISIBLE);
        webView.loadDataWithBaseURL("file:///android_asset/", contentHtml, "text/html", "UTF-8", null);
    }

    private String fixPostContent(String content) {
        // Buat variabel bantu
        String finalHTML = "";
        String trim;

        // Split content menjadi berdasarkan new line
        String[] strings = content.split("\n");

        // Tambahkan <p> pada setiap paragraf
        for (String paragraf : strings) {
            trim = paragraf.trim();

            if (!trim.isEmpty()) {
                if (trim.startsWith("<em>") ||
                        trim.startsWith("<i>") ||
                        trim.startsWith("<b>") ||
                        trim.startsWith("<strong>")) paragraf = "<p>" + paragraf + "</p>";
                else if (!trim.startsWith("<")) paragraf = "<p>" + paragraf + "</p>";
                finalHTML += paragraf + "\n";
            }
        }

        // Berikan header HTML
        finalHTML = "<meta content=\"width=device-width, initial-scale=1\" name=\"viewport\">" +
                "<link href=\"style.css\" rel=\"stylesheet\"> <body>" + finalHTML + "</body>";

        return finalHTML;
    }

    private MaterialDialog.ListCallback dialogTagCallback = new MaterialDialog.ListCallback() {
        @Override
        public void onSelection(MaterialDialog materialDialog, View view, int i, CharSequence charSequence) {
            String tagName = postDetail.tags.get(i).name;
            String tagSlug = postDetail.tags.get(i).slug;

            Intent intent = new Intent(activity, MainActivity.class);
            intent.putExtra(MainActivity.LIST_TITLE, "#" + Html.fromHtml(tagName).toString());
            intent.putExtra(MainActivity.LIST_SLUG, tagSlug);
            intent.putExtra(MainActivity.LIST_TYPE, MainActivity.TAG);
            activity.startActivity(intent);
        }
    };
}
