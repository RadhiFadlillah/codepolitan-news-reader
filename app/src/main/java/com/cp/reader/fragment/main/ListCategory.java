package com.cp.reader.fragment.main;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.cp.reader.R;
import com.cp.reader.activity.MainActivity;
import com.cp.reader.adapter.CategoryAdapter;
import com.cp.reader.model.Category;
import com.cp.reader.web.ApiService;
import com.squareup.otto.Subscribe;
import icepick.Icepick;
import icepick.State;
import org.parceler.Parcels;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import java.util.ArrayList;
import java.util.List;

public class ListCategory extends BaseFragment {
    @Bind(R.id.progress_bar) ProgressBar progressBar;
    @Bind(R.id.recycler_view) RecyclerView recyclerView;

    private ApiService api;
    private CategoryAdapter adapter;

    @State int currentPage = 1;
    @State boolean isLoading = false;
    @State int footerType = CategoryAdapter.NO_FOOTER;
    private List<Category.Result> listCategory = new ArrayList<Category.Result>();

    private static final String ARG_LIST_CATEGORY = "ARG-LIST-CATEGORY";

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Icepick.saveInstanceState(this, outState);
        outState.putParcelable(ARG_LIST_CATEGORY, Parcels.wrap(listCategory));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate and inject view
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        ButterKnife.bind(this, view);

        // Ambil api service
        api = application.getApi();

        // Buat layout manager untuk RecyclerView
        LinearLayoutManager layoutManager = new LinearLayoutManager(activity);

        // Ambil data dari saved instance kalau ada
        if (savedInstanceState != null) {
            Icepick.restoreInstanceState(this, savedInstanceState);
            listCategory = Parcels.unwrap(savedInstanceState.getParcelable(ARG_LIST_CATEGORY));
        }

        // Pastikan bahwa list kategori tidak null
        if (listCategory == null) listCategory = new ArrayList<Category.Result>();

        // Buat adapter
        adapter = new CategoryAdapter(listCategory) {
            @Override
            public void loadData() {
                loadCategory();
            }

            @Override
            public void itemClick(int position) {
                isLoading = false;
                Category.Result category = listCategory.get(position);
                activity.startListFragment(
                        Html.fromHtml(category.name).toString(),
                        category.slug,
                        MainActivity.CATEGORY
                );
            }
        };

        // Atur footer adapter
        adapter.setFooterType(footerType);

        // Atur recycler view
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        // Load daftar kategori kalau perlu
        if (listCategory.isEmpty()) {
            if (isLoading) progressBar.setVisibility(View.VISIBLE);
            else loadCategory();
        }

        return view;
    }

    private void loadCategory() {
        // Kalau masih loading, hentikan proses
        if (isLoading) return;

        // Tandai bahwa proses loading dimulai
        isLoading = true;

        // Kalau belum ada kategori yang terdaftar, tampilkan progress
        if (listCategory.isEmpty()) progressBar.setVisibility(View.VISIBLE);

        // Ambil data dari internet
        api.getAvailableCategory(currentPage, new Callback<Category>() {
            @Override
            public void success(Category result, Response response) {
                application.getEventBus().post(result);
            }

            @Override
            public void failure(RetrofitError error) {
                application.getEventBus().post(error);
            }
        });
    }

    @Subscribe
    public void getListSuccess(Category category) {
        // Tandai bahwa proses loading selesai
        isLoading = false;

        // Sembunyikan progress bar
        progressBar.setVisibility(View.GONE);

        // Kalau kode hasil API bukan 200,
        // berarti tidak ada lagi data yang bisa diambil
        if (category.code != 200) {
            footerType = CategoryAdapter.NO_FOOTER;
            adapter.setFooterType(footerType);
            return;
        }

        // Masukkan daftar baru ke dalam list
        listCategory.addAll(category.result);

        // Tampilkan ke dalam list
        adapter.replaceData(listCategory);

        // Inkremen nomor halaman
        currentPage++;

        // Aktifkan progress pada list
        footerType = CategoryAdapter.FOOTER_PROGRESS;
        adapter.setFooterType(footerType);
    }

    @Subscribe
    public void getListFailed(RetrofitError error) {
        // Tandai bahwa proses loading selesai
        isLoading = false;

        // Sembunyikan progress bar
        progressBar.setVisibility(View.GONE);

        // Atur footer list
        if (listCategory.isEmpty()) {
            footerType = CategoryAdapter.NO_FOOTER;
            Toast.makeText(activity, R.string.fail_to_fetch_data, Toast.LENGTH_SHORT).show();
        } else footerType = CategoryAdapter.FOOTER_RETRY;
        adapter.setFooterType(footerType);
    }

    @Override
    public void resetData() {
        // Cek kalau masih loading
        if (isLoading) {
            Toast.makeText(activity, R.string.is_fetching_data, Toast.LENGTH_SHORT).show();
            return;
        }

        // Ubah parameter ke nilai default
        listCategory = new ArrayList<Category.Result>();
        footerType = CategoryAdapter.NO_FOOTER;
        currentPage = 1;

        // Reset adapter
        adapter.replaceData(listCategory);
        adapter.setFooterType(footerType);

        // Load kategori baru
        loadCategory();
    }
}
