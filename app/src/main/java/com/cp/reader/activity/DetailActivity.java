package com.cp.reader.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.cp.reader.CPApplication;
import com.cp.reader.R;
import com.cp.reader.fragment.adapter.DetailPagerAdapter;
import com.cp.reader.fragment.detail.FragmentDetail;
import com.cp.reader.model.Bookmark;
import com.cp.reader.model.Post;
import io.realm.Realm;
import org.parceler.Parcels;

import java.util.List;

public class DetailActivity extends AppCompatActivity {
    @Bind(R.id.collapsing_toolbar) CollapsingToolbarLayout collapsingToolbar;
    @Bind(R.id.toolbar) Toolbar toolbar;
    @Bind(R.id.img_header) ImageView imgHeader;
    @Bind(R.id.view_pager) ViewPager viewPager;

    private Realm realm;
    private MenuItem menuBookmark;
    private List<Post.Result> listPost;
    private List<Bookmark> listBookmark;
    private DetailPagerAdapter adapter;

    private boolean isBookmark;
    private boolean hasHeader;

    public static final String POST_POSITION = "POST-POSITION";
    public static final String LIST_POST = "LIST-POST";
    public static final String HAS_HEADER = "HAS-HEADER";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);

        // Hilangkan background, untuk meminimalkan overdraw
        getWindow().setBackgroundDrawable(null);

        // Ambil realm
        realm = ((CPApplication) getApplication()).getRealm();

        // Ambil argumen
        int selectedPosition = getIntent().getIntExtra(POST_POSITION, 0);
        Parcelable parcelPost = getIntent().getParcelableExtra(LIST_POST);
        hasHeader = getIntent().getBooleanExtra(HAS_HEADER, true);

        // Unparse parcelable
        if (parcelPost != null) listPost = Parcels.unwrap(parcelPost);
        else listBookmark = realm.where(Bookmark.class).findAllSorted("id", false);

        // Atur toolbar
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(navigationOnClickListener);

        // Buat menu toolbar
        if (listPost != null) {
            toolbar.inflateMenu(R.menu.detail);
            toolbar.setOnMenuItemClickListener(menuItemClickListener);
        }

        // Tampilkan image header
        loadImageHeader(selectedPosition);

        // Ambil status bookmark untuk halaman ini
        isBookmark = getStatusBookmark(selectedPosition);

        // Atur icon bookmark
        menuBookmark = toolbar.getMenu().findItem(R.id.menu_bookmark);
        setBookmarkIcon();

        // Buat adapter
        adapter = new DetailPagerAdapter(getSupportFragmentManager(), hasHeader);
        if (listPost != null) adapter.setPost(listPost);
        else adapter.setBookmark(listBookmark);

        // Atur view pager
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(pageChangeListener);
        viewPager.setCurrentItem(selectedPosition);
    }

    private void loadImageHeader(int position) {
        if (!hasHeader) {
            imgHeader.setVisibility(View.GONE);
            return;
        }

        // Ambil link image
        String imgUrl;
        if (listPost != null) imgUrl = listPost.get(position).thumbnailSmall;
        else imgUrl = listBookmark.get(position).getThumbnailSmall();

        // Ubah link image jadi besar
        imgUrl = imgUrl.replace("-150x150", "");

        // Load image dengan glide
        Glide.with(this)
                .load(imgUrl)
                .placeholder(imgHeader.getDrawable())
                .centerCrop()
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imgHeader);
    }

    private boolean getStatusBookmark(int position) {
        // Kalau list post null, pasti bookmark
        if (listPost == null) return true;

        // Ambil data bookmark dari realm
        int n = realm.where(Bookmark.class).equalTo("id", listPost.get(position).id).findAll().size();
        return n != 0;
    }

    private void setBookmarkIcon() {
        if (menuBookmark == null) return;
        if (isBookmark) menuBookmark.setIcon(R.drawable.ic_bookmark_white);
        else menuBookmark.setIcon(R.drawable.ic_bookmark_border_white);
    }

    private ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

        @Override
        public void onPageSelected(int position) {
            // Tampilkan image
            loadImageHeader(position);

            // Ambil status bookmark untuk halaman ini
            isBookmark = getStatusBookmark(position);

            // Atur icon bookmark
            setBookmarkIcon();
        }

        @Override
        public void onPageScrollStateChanged(int state) {}
    };

    private View.OnClickListener navigationOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };

    private Toolbar.OnMenuItemClickListener menuItemClickListener = new Toolbar.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(MenuItem item) {
            int currentItem = viewPager.getCurrentItem();
            Post.Result post = listPost.get(currentItem);

            switch (item.getItemId()) {
                case R.id.menu_bookmark:
                    // Kalau halaman ini sudah pernah di bookmark,
                    // maka hapus bookmark dari database
                    if (isBookmark) {
                        // Cari bookmark di database
                        Bookmark bookmark = realm.where(Bookmark.class).equalTo("id", post.id).findFirst();

                        // Hapuskan dari database
                        realm.beginTransaction();
                        bookmark.removeFromRealm();
                        realm.commitTransaction();

                        // Tampilkan pesan
                        Toast.makeText(DetailActivity.this, R.string.bookmark_removed, Toast.LENGTH_SHORT).show();
                    }

                    // Kalau belum pernah, maka simpan halaman ini ke bookmark
                    else {
                        // Buat data bookmark baru
                        Bookmark bookmark = new Bookmark();
                        bookmark.setId(post.id);
                        bookmark.setTitle(post.title);
                        bookmark.setDate(post.date);
                        bookmark.setLink(post.link);
                        bookmark.setThumbnailSmall(post.thumbnailSmall);
                        bookmark.setThumbnailMedium(post.thumbnailMedium);
                        bookmark.setHasHeader(hasHeader);

                        // Simpan data bookmark ke database
                        realm.beginTransaction();
                        realm.copyToRealm(bookmark);
                        realm.commitTransaction();

                        // Tampilkan pesan
                        Toast.makeText(DetailActivity.this, R.string.bookmark_added, Toast.LENGTH_SHORT).show();
                    }

                    // Ubah status bookmark dan icon
                    isBookmark = !isBookmark;
                    setBookmarkIcon();
                    return true;

                case R.id.menu_refresh:
                    FragmentDetail fragment = (FragmentDetail) adapter.getRegisteredFragment(currentItem);
                    fragment.resetData();
                    return true;

                case R.id.menu_share:
                    Intent shareIntent = new Intent();
                    shareIntent.setType("text/plain");
                    shareIntent.setAction(Intent.ACTION_SEND);
                    shareIntent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(post.title) + "\n" + post.link);
                    startActivity(Intent.createChooser(shareIntent, getResources().getText(R.string.share_article)));
                    return true;

                case R.id.menu_open:
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(post.link)));
                    return true;
            }

            return false;
        }
    };
}
