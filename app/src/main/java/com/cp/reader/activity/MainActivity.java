package com.cp.reader.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.cp.reader.R;
import com.cp.reader.fragment.main.FragmentHome;
import com.cp.reader.fragment.main.ListBookmark;
import com.cp.reader.fragment.main.ListPost;

public class MainActivity extends AppCompatActivity {
    @Bind(R.id.main_toolbar) Toolbar toolbar;
    @Bind(R.id.tab_layout) TabLayout tabLayout;
    @Bind(R.id.drawer_layout) DrawerLayout drawerLayout;
    @Bind(R.id.navigation_view) NavigationView navigationView;

    public static final String LIST_TITLE = "LIST-TITLE";
    public static final String LIST_SLUG = "LIST-SLUG";
    public static final String LIST_TYPE = "LIST-TYPE";

    public static final int CATEGORY = 1;
    public static final int TAG = 2;
    public static final int TYPE = 3;
    public static final int TYPE_NO_HEADER = 4;
    public static final int SEARCH = 5;
    public static final int BOOKMARK = 6;

    private static final int ABOUT_CODEPOLITAN = 0;
    private static final int ABOUT_KARYA_LOKAL = 1;
    private static final int ABOUT_NYANKODMAGZ = 2;
    private static final int ABOUT_PUSAKA_CMS = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        // Hilangkan background, untuk meminimalkan overdraw
        getWindow().setBackgroundDrawable(null);

        // Atur navigation view
        navigationView.setNavigationItemSelectedListener(navItemSelectedListener);

        // Ambil argumen
        String title = getIntent().getStringExtra(LIST_TITLE);
        String slug = getIntent().getStringExtra(LIST_SLUG);
        int listType = getIntent().getIntExtra(LIST_TYPE, 0);

        // Buat fragment
        Fragment fragment;
        switch (listType) {
            case CATEGORY: fragment = ListPost.newInstance(title, slug, ListPost.BY_CATEGORY, true); break;
            case TAG: fragment = ListPost.newInstance(title, slug, ListPost.BY_TAG, true); break;
            case TYPE: fragment = ListPost.newInstance(title, slug, ListPost.BY_TYPE, true); break;
            case TYPE_NO_HEADER: fragment = ListPost.newInstance(title, slug, ListPost.BY_TYPE, false); break;
            case SEARCH: fragment = ListPost.newInstance(title, slug, ListPost.SEARCH, true); break;
            case BOOKMARK: fragment = new ListBookmark(); break;
            default: fragment = new FragmentHome();
        }

        // Tampilkan fragment
        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .commit();
        }
    }

    public Toolbar getToolbar() {
        return toolbar;
    }

    public TabLayout getTabLayout() {
        return tabLayout;
    }

    public void showNavDrawer() {
        drawerLayout.openDrawer(Gravity.LEFT);
    }

    public void lockNavDrawer(boolean isLocked) {
        if (isLocked) drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        else drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(Gravity.LEFT)) {
            drawerLayout.closeDrawer(Gravity.LEFT);
            return;
        }
        
        super.onBackPressed();
    }

    private NavigationView.OnNavigationItemSelectedListener navItemSelectedListener = new NavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(MenuItem item) {
            drawerLayout.closeDrawer(Gravity.LEFT);

            switch (item.getItemId()) {
                case R.id.drawer_bookmark:
                    startListFragment(null, null, BOOKMARK);
                    return true;

                case R.id.drawer_nyankomik:
                    startListFragment("Nyankomik", "nyankomik", TYPE);
                    return true;

                case R.id.drawer_meme:
                    startListFragment("Meme", "meme", TYPE_NO_HEADER);
                    return true;

                case R.id.drawer_quotes:
                    startListFragment("Quotes", "quotes", TYPE_NO_HEADER);
                    return true;

                case R.id.drawer_karyalokal:
                    showAbout(ABOUT_KARYA_LOKAL);
                    return true;

                case R.id.drawer_nyankodmagz:
                    showAbout(ABOUT_NYANKODMAGZ);
                    return true;

                case R.id.drawer_pusaka_cms:
                    showAbout(ABOUT_PUSAKA_CMS);
                    return true;

                case R.id.drawer_about:
                    showAbout(ABOUT_CODEPOLITAN);
                    return true;
            }

            return false;
        }
    };

    public void startListFragment(String title, String slug, int type) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(MainActivity.LIST_TITLE, title);
        intent.putExtra(MainActivity.LIST_SLUG, slug);
        intent.putExtra(MainActivity.LIST_TYPE, type);
        startActivity(intent);
    }

    private void showAbout(int about) {
        int logo, content;
        final String websiteURL;

        switch (about) {
            case ABOUT_KARYA_LOKAL:
                logo = R.drawable.logo_karyalokal_round;
                content = R.string.content_karyalokal;
                websiteURL = "http://www.karyalokal.com/";
                break;

            case ABOUT_NYANKODMAGZ:
                logo = R.drawable.logo_nyankodmagz_round;
                content = R.string.content_nyankodmagz;
                websiteURL = "http://www.nyankod.com/";
                break;

            case ABOUT_PUSAKA_CMS:
                logo = R.drawable.logo_pusakacms_round;
                content = R.string.content_pusaka_cms;
                websiteURL = "http://www.pusakacms.org/";
                break;

            default:
                logo = R.drawable.logo_codepolitan;
                content = R.string.content_codepolitan;
                websiteURL = "http://www.codepolitan.com/";
                break;
        }

        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .customView(R.layout.dialog_about, true)
                .positiveText(R.string.caption_website)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(websiteURL)));
                    }
                }).build();

        View dialogView = dialog.getCustomView();
        if (dialogView == null) return;

        ImageView imgLogo = (ImageView) dialogView.findViewById(R.id.img_logo);
        TextView txtContent = (TextView) dialogView.findViewById(R.id.txt_content);

        imgLogo.setImageResource(logo);
        txtContent.setText(content);

        dialog.show();
    }
}
